// Get the packages we need
var express = require('express');
var routescan = require('express-routescan');

// Create our Express application
var app = express();

// Use environment defined port or 3000
var port = process.env.PORT || 3000;

// Create our Express router
var router = express.Router();
// Load all Routes
routescan(app);

app.use(express.static(process.cwd() + '/public'));

// Start the server
app.listen(port);
console.log('Starting server on port ' + port);