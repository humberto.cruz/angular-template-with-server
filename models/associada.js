"use strict";

module.exports = function(sequelize, DataTypes) {
	var Associada = sequelize.define("Associada", {
		nome: DataTypes.STRING
	}, {
		tableName: 'associadas',
		classMethods: {
			associate: function(models) {
				Associada.hasMany(models.Boleto)
			}
		}
	});

	return Associada;
};