"use strict";

module.exports = function(sequelize, DataTypes) {
	var Boleto = sequelize.define("Boleto", {
		valor: DataTypes.FLOAT
	}, {
		tableName: 'boletos',
		classMethods: {
			associate: function(models) {
				Boleto.belongsTo(models.Associada)
			}
		}
	});

	return Boleto;
};