'use strict';

module.exports = {
	'/': {
		methods: ['get'],
		fn: function(req, res, next) {
			res.render('index.jade');
		}
	}
}
