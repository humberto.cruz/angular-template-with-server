'use strict';

var models = require('../models')
	, defaults = {
		limit: 25,
		page: 1
	};

module.exports = {
	'/api/associadas': {
		methods: ['get'],
		fn: function(req, res, next) {
			var query = req['query'];
			var limit = query.limit || defaults['limit'];
			var page = query.page || defaults['page'];
			var offset = (page-1) * limit;
			var counter = 0;
			models.Associada.count({
			}).then(function(count) {
				counter = count;
			});
			models.Associada.findAll({
				limit: limit,
				offset: offset
			}).then(function(associadas) {
				associadas['count'] = counter;
				var result = {
					data: associadas,
					counter: counter
				};
				res.json(result);
			});
		}
	}
}
