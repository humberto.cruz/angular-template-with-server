var myApp = angular.module('myApp', []);

myApp.controller('mainController',[ '$scope','$http', function($scope, $http){
	
	$scope.appName = 'Angular App';
	$http.get('/api/associadas')
		.success(function(data){
			$scope.associadas = data;
	});
	
}]);